// IMPORT dice history model  vào controller
const diceHistoryModel = require('../model/diceHistoryModel');

//  Khai báo mongoose
const mongoose = require('mongoose');

// CREATE A DICE HISTORY
const createHistory = (request, response) => {

    //B1: thu thập dữ liệu
    let bodyRequest = request.body;

    //B2: điều kiện dữ liệu
    if (!bodyRequest.user) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "user is required"
        })
    }
    if (!(Number.isInteger(bodyRequest.dice) && bodyRequest.dice > 0)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "dice is not valid"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    let createDiceHistory = {
        _id: mongoose.Types.ObjectId(),
        user: mongoose.Types.ObjectId(),
        dice: bodyRequest.dice
    }

    diceHistoryModel.create(createDiceHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Create dice history success",
                data: data
            })
        }
    })
}


// LẤY TẤT CẢ DICE HISTORY
const getAllDiceHistory = (request, response) => {
    //B1: thu thập dữ liệu
    let user = request.query.user;
    let condition = {};
    if (user) {
        condition.user = user;
    }
    console.log(user);
    //B2: validate dữ liệu
    //B3: thao tác với cơ sở dữ liệu
    diceHistoryModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get all user success",
                data: data
            })
        }
    })
}


// LẤY DICE HISTORY THEO ID
const getDiceHistoryById = (request, response) => {
    //B1: thu thập dữ liệu
    let historyId = request.params.historyId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(historyId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "Dice History ID is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    diceHistoryModel.findById(historyId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get Dice History by id success",
                data: data
            })
        }
    })
}


//UPDATE A USER
const updateDiceHistoryById = (request, response) => {
    //B1: thu thập dữ liệu
    let historyId = request.params.historyId;
    let bodyRequest = request.body;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(historyId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "Dice History ID is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    let updateDiceHistory = {
        user: bodyRequest.user,
        dice: bodyRequest.dice
    }
    diceHistoryModel.findByIdAndUpdate(historyId, updateDiceHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update Dice History success",
                data: data
            })
        }
    })
}


// DELETE A USER
const deleteDiceHistoryById = (request, response) => {
    //B1: thu thập dữ liệu
    let historyId = request.params.historyId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(historyId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Drink Id is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    diceHistoryModel.findOneAndDelete(historyId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Delete Dice History success"
            })
        }
    })
}
module.exports = { createHistory, getAllDiceHistory, getDiceHistoryById, updateDiceHistoryById, deleteDiceHistoryById };