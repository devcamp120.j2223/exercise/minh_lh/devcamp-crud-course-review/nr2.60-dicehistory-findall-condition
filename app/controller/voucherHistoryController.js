// import model
const voucherHistoryModel = require('../model/voucherHistoryModel');
// import mongoose = require('mongoose');
const mongoose = require('mongoose');
// create new createvoucherHistory 
const createVoucherHistory = (request, response) => {
    //B1: thu thập dữ liệu
    let body = request.body;
    //B2: validate dữ liệu
    if (!body.user) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "user name is not valid"
        })
    }
    if (!body.prize) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "prize is not valid"
        })
    }
    //B3: truy cập sử dụng dữ liệu
    let voucherHistoryData = {
        _id: mongoose.Types.ObjectId(),
        user: mongoose.Types.ObjectId(),
        prize: mongoose.Types.ObjectId(),
    }
    voucherHistoryModel.create(voucherHistoryData, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Create voucher history success",
                data: data
            })
        }
    })
}
// getAllvoucherHistory 
const getAllVoucherHistory = (request, response) => {
    //B1: thu thập dữ liệu
    let user = request.query.user;
    let condition = {}
    if (user) {
        condition.user = user
    }
    //B2: validate dữ liệu
    //B3: Tương tác với cơ sở dữ liệu
    voucherHistoryModel.find(condition,(error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: get all voucher history success",
                data: data
            })
        }
    })
}
// get voucher by id
const getVoucherHistoryById = (request, response) => {
    //B1: thu thập dữ liệu
    let historyId = request.params.historyId;
    //B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(historyId))) {
        return response.status(500).json({
            status: "Error 500: Internal sever Error",
            message: error.message
        })
    }
    //B3: thao tác với cơ sở dữ liệu
    voucherHistoryModel.findById(historyId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: get all voucher success",
                data: data
            })
        }
    })
}
// update voucher
const getUpdateVoucherHistory = (request, response) => {
    //B1: thu thập dữ liệu
    let historyId = request.params.historyId;
    let body = request.body;
    //B2: validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(historyId))) {
        return response.status(500).json({
            status: "Error 500: Internal sever Error",
            message: error.message
        })
    }
    if (!body.name) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "name is not valid"
        })
    }
    let voucherUpdate = {
        name: body.name,
        description: body.description,
    }
    //B3: thao tác với cơ sở dữ liệu
    voucherHistoryModel.findOneAndUpdate(historyId, voucherUpdate, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update voucher History success",
                data: data
            })
        }
    })
}
// delete voucher by id
const deleteVoucherHistoryById = (request, response) => {
    //B1: thu thập dữ liệu
    let historyId = request.params.historyId;
    //B2: validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(historyId))) {
        return response.status(500).json({
            status: "Error 500: Internal sever Error",
            message: error.message
        })
    }
    //B3: thao tác với cơ sở dữ liệu
    voucherHistoryModel.findOneAndDelete(historyId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: "Success: Delete voucher History success"
            })
        }
    })
}
//export
module.exports = {
    createVoucherHistory, getAllVoucherHistory, getVoucherHistoryById, getUpdateVoucherHistory,
    deleteVoucherHistoryById
}