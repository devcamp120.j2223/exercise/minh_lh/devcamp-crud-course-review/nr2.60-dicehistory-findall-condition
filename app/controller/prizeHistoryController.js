// import model
const prizeHistoryModel = require('../model/prizeHistoryModel');
// import mongoose = require('mongoose');
const mongoose = require('mongoose');
// create new createPrizeHistory 
const createPrizeHistory = (request, response) => {
    //B1: thu thập dữ liệu
    let body = request.body;
    //B2: validate dữ liệu
    if (!body.user) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "user name is not valid"
        })
    }
    if (!body.prize) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "prize is not valid"
        })
    }
    //B3: truy cập sử dụng dữ liệu
    let prizeHistoryData = {
        _id: mongoose.Types.ObjectId(),
        user: mongoose.Types.ObjectId(),
        prize: mongoose.Types.ObjectId(),
    }
    prizeHistoryModel.create(prizeHistoryData, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Create prize history success",
                data: data
            })
        }
    })
}
// getAllPrizeHistory 
const getAllPrizeHistory = (request, response) => {
    // B1: thu thập dữ liệu
    let user = request.query.user;
    let condition = {}
    if (user) {
        condition.user = user
    }
    // B2: Validate dữ liệu
    // B3: tương tác với cơ sở dữ liệu
    prizeHistoryModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: get all prize history success",
                data: data
            })
        }
    })
}
// get prize by id
const getPrizeHistoryById = (request, response) => {
    //B1: thu thập dữ liệu
    let historyId = request.params.historyId;
    //B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(historyId))) {
        return response.status(500).json({
            status: "Error 500: Internal sever Error",
            message: error.message
        })
    }
    //B3: thao tác với cơ sở dữ liệu
    prizeHistoryModel.findById(historyId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: get all prize success",
                data: data
            })
        }
    })
}
// update prize
const getUpdatePrizeHistory = (request, response) => {
    //B1: thu thập dữ liệu
    let historyId = request.params.historyId;
    let body = request.body;
    //B2: validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(historyId))) {
        return response.status(500).json({
            status: "Error 500: Internal sever Error",
            message: error.message
        })
    }
    if (!body.name) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "name is not valid"
        })
    }
    let prizeUpdate = {
        name: body.name,
        description: body.description,
    }
    //B3: thao tác với cơ sở dữ liệu
    prizeHistoryModel.findOneAndUpdate(historyId, prizeUpdate, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update prize History success",
                data: data
            })
        }
    })
}
// delete prize by id
const deletePrizeHistoryById = (request, response) => {
    //B1: thu thập dữ liệu
    let historyId = request.params.historyId;
    //B2: validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(historyId))) {
        return response.status(500).json({
            status: "Error 500: Internal sever Error",
            message: error.message
        })
    }
    //B3: thao tác với cơ sở dữ liệu
    prizeHistoryModel.findOneAndDelete(historyId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: "Success: Delete prize History success"
            })
        }
    })
}
//export
module.exports = {
    createPrizeHistory, getAllPrizeHistory, getPrizeHistoryById, getUpdatePrizeHistory,
    deletePrizeHistoryById
}