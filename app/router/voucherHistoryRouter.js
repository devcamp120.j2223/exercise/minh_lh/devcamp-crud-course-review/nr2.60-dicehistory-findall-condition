// Khai báo thư viện express
const express = require('express');


//Import  Controller
const { createVoucherHistory, getAllVoucherHistory, getVoucherHistoryById, getUpdateVoucherHistory,
    deleteVoucherHistoryById } =
    require('../controller/voucherHistoryController');

// Tạo router
const router = express.Router();

//CREATE A Dice History
router.post('/voucher-histories', createVoucherHistory);
// get all prizes history
router.get('/voucher-histories', getAllVoucherHistory);
// get by id
router.get('/voucher-histories/:historyId', getVoucherHistoryById);
// get update Prize
router.put('/voucher-histories/:historyId', getUpdateVoucherHistory);
// deletePrizeById
router.delete('/voucher-histories/:historyId', deleteVoucherHistoryById)
//export 

//export
module.exports = router;