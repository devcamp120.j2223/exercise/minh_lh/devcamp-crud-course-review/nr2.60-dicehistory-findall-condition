// import express
const express = require('express');
// import mongoose
const mongoose = require('mongoose');
// import controller
const { createNewPrize, getAllPrize, getPrizeById, getUpdatePrize, deletePrizeById } = require('../controller/prizeController');
// gán router
const router = express.Router();
// api
router.post('/prizes', createNewPrize);
// get all prizes
router.get('/prizes', getAllPrize);
// get prize by id
router.get('/prizes/:prizeId', getPrizeById);
// get updatePrize
router.put('/prizes/:prizeId', getUpdatePrize);
// deletePrizeById
router.delete('/prizes/:prizeId', deletePrizeById)
//export 
module.exports = router;