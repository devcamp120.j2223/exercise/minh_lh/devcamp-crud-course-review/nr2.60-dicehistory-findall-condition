// khai báo mongoose
const mongoose = require('mongoose');

// khởi tạo schema
const schema = mongoose.Schema;

const PrizeHistorySchema = new schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    user: [
        {
            type: mongoose.Types.ObjectId,
            ref: "user",
            required: true,
        }
    ], 
    prize: [
        {
            type: mongoose.Types.ObjectId,
            ref: "voucher",
            required: true,
        }
    ],
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
})

//export thành một modules
module.exports = mongoose.model("prize-history", PrizeHistorySchema)